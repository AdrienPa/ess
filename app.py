from flask import Flask, render_template, jsonify
#from dotenv import load_dotenv
import DB_connect


#load_dotenv()
app = Flask(__name__)

@app.route("/", methods=["GET"])
def get_home():
    return render_template(
        "home.html",
        titre="Les entreprises de l'économie sociale et solidaire"
    )

# @app.route("/", methods=["GET"])
# def get_dashboard():
#     return jsonify()
