'''Module de création et de suppression des tables'''


import DB_connect


def creation_table():
    conn = DB_connect.connexion()
    c = conn.cursor()
    commands = (

        '''CREATE TABLE IF NOT EXISTS Entreprise (entreprise_id SERIAL,
                                            siren INTEGER,
                                            siret TEXT,
                                            denominationUniteLegale TEXT,
                                            dateDernierTraitementEtablissement TEXT,
                                            dateDernierTraitementUniteLegale TEXT,
                                            PRIMARY KEY(entreprise_id)
                                            )''',

        '''CREATE TABLE IF NOT EXISTS Taille (taille_id SERIAL,
                                        categorieEntreprise TEXT,
                                        trancheEffectifsEtablissement TEXT,
                                        trancheEffectifsUniteLegale TEXT,
                                        PRIMARY KEY (taille_id),
                                        FOREIGN KEY (taille_id)
                                            REFERENCES Entreprise (entreprise_id)
                                            ON UPDATE CASCADE ON DELETE CASCADE)''',

        '''CREATE TABLE IF NOT EXISTS Adresse (adresse_id SERIAL,
                                        numeroVoieEtablissement INTEGER,
                                        typeVoieEtablissement TEXT,
                                        libelleVoieEtablissement TEXT,
                                        codePostalEtablissement INTEGER,
                                        libelleCommuneEtablissement TEXT,
                                        PRIMARY KEY (adresse_id),
                                        FOREIGN KEY (adresse_id)
                                            REFERENCES Entreprise (entreprise_id)
                                            ON UPDATE CASCADE ON DELETE CASCADE)''',

        '''CREATE TABLE IF NOT EXISTS Coord (coord_id SERIAL,
                                            Latitude FLOAT,
                                            Longitude FLOAT,
                                            PRIMARY KEY (coord_id),
                                            FOREIGN KEY (coord_id)
                                                REFERENCES Entreprise (entreprise_id)
                                                ON UPDATE CASCADE ON DELETE CASCADE,
                                            FOREIGN KEY (coord_id)
                                                REFERENCES Adresse (adresse_id)
                                                ON UPDATE CASCADE ON DELETE CASCADE)''',

        '''CREATE TABLE IF NOT EXISTS Adresse_coord (adresseid INTEGER NOT NULL,
                                                    coordid INTEGER NOT NULL,
                                                    PRIMARY KEY (adresseid, coordid),
                                                    FOREIGN KEY (adresseid) 
                                                        REFERENCES Adresse(adresse_id)
                                                        ON UPDATE CASCADE ON DELETE CASCADE,
                                                    FOREIGN KEY (coordid) 
                                                        REFERENCES Coord(coord_id)
                                                        ON UPDATE CASCADE ON DELETE CASCADE)'''
                                                )

    for command in commands:
        c.execute(command)

    conn.commit()


# Pour ajouter une colonne adresse complète si besoin
def full_adresse():
    conn = DB_connect.connexion()
    c = conn.cursor()
    ajout_col = '''ALTER TABLE Adresse
                    ADD COLUMN IF NOT EXISTS fullAdresse TEXT;'''
    updated = '''UPDATE Adresse SET fullAdresse = CONCAT_WS(' ',
                                                            numeroVoieEtablissement,
                                                            typeVoieEtablissement,
                                                            libelleVoieEtablissement,
                                                            codePostalEtablissement,
                                                            libelleCommuneEtablissement);'''
    c.execute(ajout_col)
    c.execute(updated)
    conn.commit()


def del_table():
    conn = DB_connect.connexion()
    c = conn.cursor()
    c.execute('''DROP TABLE Entreprise CASCADE''')
    c.execute('''DROP TABLE Taille''')
    c.execute('''DROP TABLE Adresse''')
    c.execute('''DROP TABLE Coord''')
    conn.commit()


creation_table()
#full_adresse()
#del_table()
