- DB_tables permet de créer ou supprimer les tables de la base de données. Il appelle directement DB_connect qui sert à créer la base de données et démarrer la connexion.
- insert_total permet d'insérer les données dans la base de données et de géolocaliser les adresses. Il contient également une fonction de mesure de performance pour ces fonctions.
- superset_config contient la variable de clé d'API Mapbox qui permet de générer les cartes dans Apache Superset. Pour obtenir une clé, se rendre sur le site https://www.mapbox.com/, créer un compte et générer une clé.

Pour installer le projet, utiliser un environnement virtuel comme pipenv et lancer la commande "pipenv install".
Pour accéder à Superset, utiliser la commande "superset run".
Pour voir le résultat du projet, utiliser Flask et la commande "flask run".