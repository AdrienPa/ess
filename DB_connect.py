'''Module de connexion à la base de données'''


import psycopg2


def connexion():
    conn = None
    try:
        conn = psycopg2.connect(dbname="ESS",
                                user="economie",
                                password="sociale",
                                host="localhost",
                                port="5432")
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    return conn


def deconnexion():
    conn = connexion()
    c = conn.cursor()
    c.close()
    conn.close()


connexion()
deconnexion()