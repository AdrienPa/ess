'''Module d'insertion des données et de géocodage des adresses'''


from api_insee import ApiInsee
from api_insee.criteria import Field
import timeit
from tqdm import tqdm
import geocoder
import DB_connect


def get_all():
    conn = DB_connect.connexion()
    c = conn.cursor()
    api = ApiInsee(
            key="QMBbAs96fuzPH2iTsfYvy6qzADca",
            secret="256KQm0WKFLm662MTatM4ZT7PoAa"
        )

    champs = ["siren",
                    "siret",
                    "dateDernierTraitementEtablissement",
                    "dateDernierTraitementUniteLegale",
                    "denominationUniteLegale",
                    "categorieEntreprise",
                    "numeroVoieEtablissement",
                    "typeVoieEtablissement",
                    "libelleVoieEtablissement",
                    "codePostalEtablissement",
                    "libelleCommuneEtablissement",
                    "trancheEffectifsEtablissement",
                    "trancheEffectifsUniteLegale"
                    ]

    request = api.siret(q=(
        Field('economieSocialeSolidaireUniteLegale', 'O'),
        Field('codePostalEtablissement', 38130)),
        champs=champs,
        curseur='*'
        )

    '''Recupérer uniquement les éléments dont le code postal est 38130 selon le paramètre :
    Field('codePostalEtablissement', '38130'), et les compter'''

    for (page_index, page_result) in enumerate(request.pages(nombre=1000)):
        #time.sleep(2.3)
        for value in page_result["etablissements"]:
            siren = value["siren"]
            siret = value["siret"]
            nom = value["uniteLegale"]["denominationUniteLegale"]
            ddtul = value["uniteLegale"]["dateDernierTraitementUniteLegale"]
            ddte = value["dateDernierTraitementEtablissement"]

            catentreprise = value["uniteLegale"]["categorieEntreprise"]
            tee = value["trancheEffectifsEtablissement"]
            teul = value["uniteLegale"]["trancheEffectifsUniteLegale"]

            numero = value["adresseEtablissement"]["numeroVoieEtablissement"]
            typevoie = value["adresseEtablissement"]["typeVoieEtablissement"]
            nomrue = value["adresseEtablissement"]["libelleVoieEtablissement"]
            codepostal = value["adresseEtablissement"]["codePostalEtablissement"]
            commune = value["adresseEtablissement"]["libelleCommuneEtablissement"]

            #n+=1
            #print(n, catentreprise)
            #print(catentreprise)


            c.execute('''INSERT INTO Entreprise (
                    siren, siret, denominationUniteLegale, dateDernierTraitementEtablissement, dateDernierTraitementUniteLegale
                ) VALUES (%s,%s,%s,%s,%s)''', (siren, siret, nom, ddtul, ddte))
            c.execute('''INSERT INTO Taille (
                    categorieEntreprise, trancheEffectifsEtablissement, trancheEffectifsUniteLegale
                ) VALUES (%s,%s,%s)''', (catentreprise, tee, teul))
            c.execute('''INSERT INTO Adresse (
                    numeroVoieEtablissement, typeVoieEtablissement, libelleVoieEtablissement, codePostalEtablissement, libelleCommuneEtablissement
                ) VALUES (%s,%s,%s,%s,%s)''', (numero, typevoie, nomrue, codepostal, commune))

    conn.commit()


def concat_adresse():
    conn = DB_connect.connexion()
    c = conn.cursor()
    c.execute('''SELECT CONCAT_WS(' ', numeroVoieEtablissement, typeVoieEtablissement, libelleVoieEtablissement, codePostalEtablissement, libelleCommuneEtablissement) FROM Adresse ORDER BY adresse_id''')
    newadresse = c.fetchall()
    #print(newadresse)
    return newadresse


def get_coord():
    conn = DB_connect.connexion()
    c = conn.cursor()
    n = 0
    for elt in tqdm(concat_adresse()):
        try:
            g = geocoder.arcgis(elt[0])
            geo = g.json
            lat = geo["lat"]
            lon = geo["lng"]
            print(lat, lon)

            c.execute('''INSERT INTO Coord (Latitude, Longitude) VALUES (%s, %s)''', (lat, lon))
        except:
            n+=1
        conn.commit()
        print(n)


def mesure_temps():
    print(timeit.timeit(concat_adresse, number=100))
    print(timeit.timeit(get_all, number=5))
    #print(timeit.timeit(get_coord, number=5))


get_all()
concat_adresse()
get_coord()
#mesure_temps()
